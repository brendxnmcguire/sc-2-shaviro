#Tele*visor*

We've all done it once, binge watched TV solely to find out what happens next or if our favorite character will die or live. 
However, Shaviro writes that the tantalizing nature of TV is the reason it is so insidious, for he states:
"It�s silly to think that anyone is brainwashed by TV. It doesn�t constrain us, or perpetrate violence upon us. Much more subtly and insidiously, TV draws us into discourse, absorbs us into the network."
This might be concerning to some, that the subconcious is so effected by what we view on the TV, but what should be more concerning is the message it gives us- the subtitles.

![](http://0.media.collegehumor.cvcdn.com/31/30/e97876caadda6be4737398aa090480ee.jpg)

* * *

####Movie Subtitles
These are the most accurate subtitles found, and directly from the script are transcribed to allow those who cannot hear glean the plot of a movie.
Transcription of the script is extremely similar to transcription occuring in your body right now. DNA copies itself to RNA which in turn makes amino acids and then proteins.
Movie transcripts do the same, turn the readable script, although not close enough to be paired with visuals, into a usable source to be consumed and used by those who watch movies.

[People do not usually appreciate all subtitle work however](http://whatculture.com/film/10-movie-names-absolutely-arbitrary-subtitles)

* * *

####TV Subtitles 
The less accurate subtitles still show us something about how ideas and visuals become text onto the screen. Usually taken from whatever script is available from the set and/or intepretations. This is why Shaviro says the folowing:
"No matter what words you utter, those words will have been anticipated somewhere in the chains of discourse� There is no place of indemnity that would somehow be free of these constraints."
Why are swears important for subtitles?

[![Swear Word VSauce](http://img.youtube.com/vi/Dd7dQh8u4Hc/0.jpg)](http://www.youtube.com/watch?v=Dd7dQh8u4Hc)

The swearwords have been very controversial when it comes to the subtitle use of a TV show and gives insight on the values that specific corporations want the consumer to think they have.
These percieved views have become Shaviro's "uttered words" and are inticipated by people who root for the company. There was no place where they could have been free from that judgement. 
This is very similar to Kelly's perception of making arguments from *within* the system that you are percieving.

* * * 

Whatever your position on subtitles, they do convey a message that brings people together and connects them through an artistic medium, so what is it then besides Kevin Kelly's technium?

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqbU7Ts41rpO4wmjLHnMm_GIdCl4zqiMHiu3FT18rKP5QGwFfB)
